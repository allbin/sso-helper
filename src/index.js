var axios = require('axios');

let storage = typeof localStorage !== 'undefined' ? localStorage : null;

function InvalidCredentialsError(message) {
    this.name = 'InvalidCredentialsError';
    this.message = message || '';
}
InvalidCredentialsError.prototype = Error.prototype;

function InvalidTokenError(message) {
    this.name = 'InvalidTokenError';
    this.message = message || '';
}
InvalidTokenError.prototype = Error.prototype;

function JWT(token) {
    var token_parts;

    this.raw_token = token;

    if (Buffer.isBuffer(token)) {
        token_parts = token.toString().split('.');
    } else {
        token_parts = token.split('.');
    }

    this.header = JSON.parse(JWT.base64urlDecode(token_parts[0]));
    this.claims = JSON.parse(JWT.base64urlDecode(token_parts[1]));
    this.signature = token_parts[2];

    return this;
}

JWT.prototype.getRaw = function() {
    return this.raw_token;
};

JWT.prototype.getClaims = function() {
    return this.claims;
};

JWT.prototype.getClaim = function(claim) {
    return this.claims ? this.claims[claim] : null;
};

JWT.prototype.isValid = function() {
    var now = Date.now() / 1000;
    return (
        this.claims.exp > now &&
        (this.claims.nbf ? this.claims.nbf <= now : true)
    );
};

JWT.prototype.needsRenewal = function() {
    var now = Date.now() / 1000;
    var life = this.claims.exp - this.claims.nbf;
    var left = life - (now - this.claims.nbf);
    var pct = left / life;

    return pct < 0.2;
};

JWT.base64urlDecode = function(str) {
    return new Buffer(JWT.base64urlUnescape(str), 'base64').toString();
};

JWT.base64urlUnescape = function(str) {
    str += new Array(5 - (str.length % 4)).join('=');
    return str.replace(/-/g, '+').replace(/_/g, '/');
};

JWT.base64urlEncode = function(str) {
    return JWT.base64urlEscape(new Buffer(str).toString('base64'));
};

JWT.base64urlEscape = function(str) {
    return str
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
};

function SSO(service, options) {
    var optionDefaults = {
        login_uri: 'https://login.allbin.se',
        token_provider_uri: 'https://sso.allbin.se',
        renewal_check_interval: 30,
        jwt_acquire_callback: null,
        jwt_renew_callback: null,
        jwt_release_callback: null,
    };

    this.jwt = null;
    this.service = service;
    this.options = Object.assign({}, optionDefaults, options || {});
    this.token_renewal_interval = null;

    this.clearRenewalInterval = () => {
        if (this.token_renewal_interval) {
            clearInterval(this.token_renewal_interval);
            this.token_renewal_interval = null;
        }
    };

    this.checkRenew = () => {
        if (!this.jwt) {
            return;
        }
        if (!this.jwt.isValid()) {
            console.log('[SSO] JWT token is no longer valid');
            this.clearJWT(false);
            return;
        }
        if (this.jwt.needsRenewal()) {
            this.renewToken()
                .then(() => {
                    console.log('[SSO] JWT token renewal successful');
                    if (this.options.jwt_renew_callback) {
                        this.options.jwt_renew_callback(this.jwt);
                    }
                })
                .catch(err => {
                    console.error('[SSO] JWT renewal failed: ' + err.stack);
                    if (!(err.code === 'ECONNABORTED')) {
                        // don't throw away the token on renewal timeout
                        this.clearJWT(false);
                    }
                });
        }
    };

    this.startRenewalInterval = () => {
        this.clearRenewalInterval();
        this.token_renewal_interval = setInterval(() => {
            this.checkRenew();
        }, this.options.renewal_check_interval * 1000);
    };

    this.getSavedJWT = key => {
        if (storage) {
            var saved_token = storage.getItem(key);
            if (saved_token) {
                return new JWT(saved_token);
            }
        }
        return null;
    };

    return this;
}

SSO.prototype.getJWT = function() {
    return this.jwt;
};

SSO.prototype.setJWT = function(jwt) {
    if (!jwt) {
        throw new Error('Attempt to set null JWT: ' + jwt);
    }
    this.jwt = jwt;
    if (storage) {
        storage.setItem(this.service + '_jwt_token', jwt.getRaw());
        storage.setItem('last_jwt_token', jwt.getRaw());
    }
    console.log(
        '[SSO] Set token for service: ' + this.service + '(and last token)',
    );
    this.startRenewalInterval();
};

SSO.prototype.setJWTFromRaw = function(token) {
    this.setJWT(new JWT(token));
};

SSO.prototype.clearJWT = function(wasLogout) {
    if (this.jwt) {
        this.jwt = null;
        console.log('[SSO] Cleared token for service:' + this.service);
        if (storage) {
            storage.removeItem(this.service + '_jwt_token');
        }
    }
    this.clearRenewalInterval();
    if (this.options.jwt_release_callback) {
        this.options.jwt_release_callback(wasLogout);
    }
};

SSO.prototype.clearLastJWT = function() {
    console.log('[SSO] Cleared last token');
    if (storage) {
        storage.removeItem('last_jwt_token');
    }
};

SSO.prototype.loginWithCredentials = function(username, password) {
    return new Promise((resolve, reject) => {
        var auth_str = JWT.base64urlEncode(username + ':' + password);
        axios({
            url:
                this.options.token_provider_uri +
                '/token?service=' +
                this.service,
            headers: {
                Authorization: 'Basic ' + auth_str,
            },
        })
            .then(res => {
                if (!res.data.token) {
                    return reject(new InvalidCredentialsError());
                }
                var jwt = new JWT(res.data.token);
                if (!jwt.isValid()) {
                    return reject(
                        new InvalidTokenError('Invalid token issued?'),
                    );
                }
                if (jwt.getClaim('svc') !== this.service) {
                    return reject(
                        new InvalidTokenError(
                            'Token was not issued for this service',
                        ),
                    );
                }
                if (!jwt.getClaim('login')) {
                    this.clearJWT(false);
                    return reject(
                        new InvalidTokenError('No login claim in token'),
                    );
                }
                this.setJWT(jwt);
                return resolve(jwt);
            })
            .catch(err => {
                return reject(err);
            });
    });
};

SSO.prototype.loginWithToken = function(token) {
    return new Promise((resolve, reject) => {
        axios({
            url:
                this.options.token_provider_uri +
                '/token?service=' +
                this.service,
            headers: {
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                if (!res.data.token) {
                    return reject(new InvalidCredentialsError());
                }
                var jwt = new JWT(res.data.token);
                if (!jwt.isValid()) {
                    return reject(
                        new InvalidTokenError('Invalid token issued?'),
                    );
                }
                if (jwt.getClaim('svc') !== this.service) {
                    return reject(
                        new InvalidTokenError(
                            'Token was not issued for this service',
                        ),
                    );
                }
                if (!jwt.getClaim('login')) {
                    this.clearLastJWT();
                    this.clearJWT(false);
                    return reject(
                        new InvalidTokenError('No login claim in token'),
                    );
                }
                this.setJWT(jwt);
                return resolve(jwt);
            })
            .catch(err => {
                this.clearLastJWT();
                this.clearJWT(false);
                return reject(err);
            });
    });
};

SSO.prototype.logout = function() {
    this.clearLastJWT();
    this.clearJWT(true);
    document.location = this.options.login_uri + '/logout';
};

SSO.prototype.renewToken = function() {
    return new Promise((resolve, reject) => {
        axios({
            url:
                this.options.token_provider_uri +
                '/token?service=' +
                this.jwt.getClaim('svc'),
            headers: {
                Authorization: 'Bearer ' + this.jwt.getRaw(),
            },
            timeout: 10000,
        })
            .then(res => {
                if (!res.data.token) {
                    return reject(new InvalidCredentialsError());
                }
                var jwt = new JWT(res.data.token);
                if (!jwt.isValid()) {
                    return reject(
                        new InvalidTokenError('Invalid token issued?'),
                    );
                }
                if (jwt.getClaim('svc') !== this.service) {
                    return reject(
                        new InvalidTokenError(
                            'Token was not issued for this service',
                        ),
                    );
                }
                if (!jwt.getClaim('login')) {
                    this.clearJWT(false);
                    return reject(
                        new InvalidTokenError('No login claim in token'),
                    );
                }
                this.setJWT(jwt);
                return resolve(jwt);
            })
            .catch(err => {
                return reject(err);
            });
    });
};

SSO.prototype.requestPasswordReset = function(username) {
    return new Promise((resolve, reject) => {
        axios({
            url: this.options.token_provider_uri + '/reset/request',
            method: 'POST',
            data: {
                username: username,
            },
        })
            .then((/* res */) => {
                return resolve();
            })
            .catch(err => {
                return reject(err);
            });
    });
};

SSO.prototype.performPasswordReset = function(username, password, code) {
    return new Promise((resolve, reject) => {
        axios({
            url: this.options.token_provider_uri + '/reset/perform',
            method: 'POST',
            data: {
                username: username,
                password: password,
                code: code,
            },
        })
            .then(res => {
                return resolve(res.data);
            })
            .catch(err => {
                return reject(err);
            });
    });
};

SSO.prototype.isLoggedIn = function() {
    if (!this.jwt) {
        return false;
    }
    if (!this.jwt.isValid()) {
        this.clearJWT(false);
        return false;
    }
    return (
        this.jwt.getClaim('svc') === this.service && this.jwt.getClaim('login')
    );
};

SSO.prototype.getServiceSpecificToken = function(service) {
    var key = service + '_jwt_token';
    var jwt = this.getSavedJWT(key);
    if (jwt) {
        if (jwt.isValid() && jwt.getClaim('svc') === service) {
            return jwt;
        }
        if (storage) {
            storage.removeItem(key);
        }
    }
    return null;
};

SSO.prototype.getLastToken = function() {
    var key = 'last_jwt_token';
    var jwt = this.getSavedJWT(key);
    if (jwt) {
        if (jwt.isValid()) {
            return jwt;
        }
        if (storage) {
            storage.removeItem(key);
        }
    }
    return null;
};

SSO.prototype.init = function() {
    return new Promise((resolve, reject) => {
        if (window) {
            var token = decodeURIComponent(
                window.location.search.replace(
                    new RegExp(
                        '^(?:.*[&\\?]' +
                            encodeURIComponent('token').replace(
                                /[.+*]/g,
                                '\\$&',
                            ) +
                            '(?:\\=([^&]*))?)?.*$',
                        'i',
                    ),
                    '$1',
                ),
            );
            if (token) {
                console.log('[SSO] JWT token acquired: ' + token);
                this.setJWTFromRaw(token);
                if (this.options.jwt_acquire_callback) {
                    this.options.jwt_acquire_callback(this.jwt);
                }
            }
        }

        var jwt = this.getServiceSpecificToken(this.service);
        if (jwt) {
            console.log(
                '[SSO] Found existing token for service: ' + this.service,
            );
            this.setJWT(jwt);
            return resolve();
        }

        jwt = this.getLastToken();
        if (jwt) {
            console.log('[SSO] Found existing token for other service');
            return this.loginWithToken(jwt.getRaw())
                .then((/* jwt */) => {
                    return resolve();
                })
                .catch(err => {
                    return reject(err);
                });
        }
        this.jwt = null;
        return resolve();
    });
};

SSO.prototype.JWT = JWT;

module.exports = SSO;
