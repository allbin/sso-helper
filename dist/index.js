'use strict';

var axios = require('axios');

var storage = typeof localStorage !== 'undefined' ? localStorage : null;

function InvalidCredentialsError(message) {
    this.name = 'InvalidCredentialsError';
    this.message = message || '';
}
InvalidCredentialsError.prototype = Error.prototype;

function InvalidTokenError(message) {
    this.name = 'InvalidTokenError';
    this.message = message || '';
}
InvalidTokenError.prototype = Error.prototype;

function JWT(token) {
    var token_parts;

    this.raw_token = token;

    if (Buffer.isBuffer(token)) {
        token_parts = token.toString().split('.');
    } else {
        token_parts = token.split('.');
    }

    this.header = JSON.parse(JWT.base64urlDecode(token_parts[0]));
    this.claims = JSON.parse(JWT.base64urlDecode(token_parts[1]));
    this.signature = token_parts[2];

    return this;
}

JWT.prototype.getRaw = function () {
    return this.raw_token;
};

JWT.prototype.getClaims = function () {
    return this.claims;
};

JWT.prototype.getClaim = function (claim) {
    return this.claims ? this.claims[claim] : null;
};

JWT.prototype.isValid = function () {
    var now = Date.now() / 1000;
    return this.claims.exp > now && (this.claims.nbf ? this.claims.nbf <= now : true);
};

JWT.prototype.needsRenewal = function () {
    var now = Date.now() / 1000;
    var life = this.claims.exp - this.claims.nbf;
    var left = life - (now - this.claims.nbf);
    var pct = left / life;

    return pct < 0.2;
};

JWT.base64urlDecode = function (str) {
    return new Buffer(JWT.base64urlUnescape(str), 'base64').toString();
};

JWT.base64urlUnescape = function (str) {
    str += new Array(5 - str.length % 4).join('=');
    return str.replace(/-/g, '+').replace(/_/g, '/');
};

JWT.base64urlEncode = function (str) {
    return JWT.base64urlEscape(new Buffer(str).toString('base64'));
};

JWT.base64urlEscape = function (str) {
    return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
};

function SSO(service, options) {
    var _this = this;

    var optionDefaults = {
        login_uri: 'https://login.allbin.se',
        token_provider_uri: 'https://sso.allbin.se',
        renewal_check_interval: 30,
        jwt_acquire_callback: null,
        jwt_renew_callback: null,
        jwt_release_callback: null
    };

    this.jwt = null;
    this.service = service;
    this.options = Object.assign({}, optionDefaults, options || {});
    this.token_renewal_interval = null;

    this.clearRenewalInterval = function () {
        if (_this.token_renewal_interval) {
            clearInterval(_this.token_renewal_interval);
            _this.token_renewal_interval = null;
        }
    };

    this.checkRenew = function () {
        if (!_this.jwt) {
            return;
        }
        if (!_this.jwt.isValid()) {
            console.log('[SSO] JWT token is no longer valid');
            _this.clearJWT(false);
            return;
        }
        if (_this.jwt.needsRenewal()) {
            _this.renewToken().then(function () {
                console.log('[SSO] JWT token renewal successful');
                if (_this.options.jwt_renew_callback) {
                    _this.options.jwt_renew_callback(_this.jwt);
                }
            }).catch(function (err) {
                console.error('[SSO] JWT renewal failed: ' + err.stack);
                if (!(err.code === 'ECONNABORTED')) {
                    // don't throw away the token on renewal timeout
                    _this.clearJWT(false);
                }
            });
        }
    };

    this.startRenewalInterval = function () {
        _this.clearRenewalInterval();
        _this.token_renewal_interval = setInterval(function () {
            _this.checkRenew();
        }, _this.options.renewal_check_interval * 1000);
    };

    this.getSavedJWT = function (key) {
        if (storage) {
            var saved_token = storage.getItem(key);
            if (saved_token) {
                return new JWT(saved_token);
            }
        }
        return null;
    };

    return this;
}

SSO.prototype.getJWT = function () {
    return this.jwt;
};

SSO.prototype.setJWT = function (jwt) {
    if (!jwt) {
        throw new Error('Attempt to set null JWT: ' + jwt);
    }
    this.jwt = jwt;
    if (storage) {
        storage.setItem(this.service + '_jwt_token', jwt.getRaw());
        storage.setItem('last_jwt_token', jwt.getRaw());
    }
    console.log('[SSO] Set token for service: ' + this.service + '(and last token)');
    this.startRenewalInterval();
};

SSO.prototype.setJWTFromRaw = function (token) {
    this.setJWT(new JWT(token));
};

SSO.prototype.clearJWT = function (wasLogout) {
    if (this.jwt) {
        this.jwt = null;
        console.log('[SSO] Cleared token for service:' + this.service);
        if (storage) {
            storage.removeItem(this.service + '_jwt_token');
        }
    }
    this.clearRenewalInterval();
    if (this.options.jwt_release_callback) {
        this.options.jwt_release_callback(wasLogout);
    }
};

SSO.prototype.clearLastJWT = function () {
    console.log('[SSO] Cleared last token');
    if (storage) {
        storage.removeItem('last_jwt_token');
    }
};

SSO.prototype.loginWithCredentials = function (username, password) {
    var _this2 = this;

    return new Promise(function (resolve, reject) {
        var auth_str = JWT.base64urlEncode(username + ':' + password);
        axios({
            url: _this2.options.token_provider_uri + '/token?service=' + _this2.service,
            headers: {
                Authorization: 'Basic ' + auth_str
            }
        }).then(function (res) {
            if (!res.data.token) {
                return reject(new InvalidCredentialsError());
            }
            var jwt = new JWT(res.data.token);
            if (!jwt.isValid()) {
                return reject(new InvalidTokenError('Invalid token issued?'));
            }
            if (jwt.getClaim('svc') !== _this2.service) {
                return reject(new InvalidTokenError('Token was not issued for this service'));
            }
            if (!jwt.getClaim('login')) {
                _this2.clearJWT(false);
                return reject(new InvalidTokenError('No login claim in token'));
            }
            _this2.setJWT(jwt);
            return resolve(jwt);
        }).catch(function (err) {
            return reject(err);
        });
    });
};

SSO.prototype.loginWithToken = function (token) {
    var _this3 = this;

    return new Promise(function (resolve, reject) {
        axios({
            url: _this3.options.token_provider_uri + '/token?service=' + _this3.service,
            headers: {
                Authorization: 'Bearer ' + token
            }
        }).then(function (res) {
            if (!res.data.token) {
                return reject(new InvalidCredentialsError());
            }
            var jwt = new JWT(res.data.token);
            if (!jwt.isValid()) {
                return reject(new InvalidTokenError('Invalid token issued?'));
            }
            if (jwt.getClaim('svc') !== _this3.service) {
                return reject(new InvalidTokenError('Token was not issued for this service'));
            }
            if (!jwt.getClaim('login')) {
                _this3.clearJWT(false);
                return reject(new InvalidTokenError('No login claim in token'));
            }
            _this3.setJWT(jwt);
            return resolve(jwt);
        }).catch(function (err) {
            return reject(err);
        });
    });
};

SSO.prototype.logout = function () {
    this.clearLastJWT();
    this.clearJWT(true);
    document.location = this.options.login_uri + '/logout';
};

SSO.prototype.renewToken = function () {
    var _this4 = this;

    return new Promise(function (resolve, reject) {
        axios({
            url: _this4.options.token_provider_uri + '/token?service=' + _this4.jwt.getClaim('svc'),
            headers: {
                Authorization: 'Bearer ' + _this4.jwt.getRaw()
            },
            timeout: 10000
        }).then(function (res) {
            if (!res.data.token) {
                return reject(new InvalidCredentialsError());
            }
            var jwt = new JWT(res.data.token);
            if (!jwt.isValid()) {
                return reject(new InvalidTokenError('Invalid token issued?'));
            }
            if (jwt.getClaim('svc') !== _this4.service) {
                return reject(new InvalidTokenError('Token was not issued for this service'));
            }
            if (!jwt.getClaim('login')) {
                _this4.clearJWT(false);
                return reject(new InvalidTokenError('No login claim in token'));
            }
            _this4.setJWT(jwt);
            return resolve(jwt);
        }).catch(function (err) {
            return reject(err);
        });
    });
};

SSO.prototype.requestPasswordReset = function (username) {
    var _this5 = this;

    return new Promise(function (resolve, reject) {
        axios({
            url: _this5.options.token_provider_uri + '/reset/request',
            method: 'POST',
            data: {
                username: username
            }
        }).then(function () /* res */{
            return resolve();
        }).catch(function (err) {
            return reject(err);
        });
    });
};

SSO.prototype.performPasswordReset = function (username, password, code) {
    var _this6 = this;

    return new Promise(function (resolve, reject) {
        axios({
            url: _this6.options.token_provider_uri + '/reset/perform',
            method: 'POST',
            data: {
                username: username,
                password: password,
                code: code
            }
        }).then(function (res) {
            return resolve(res.data);
        }).catch(function (err) {
            return reject(err);
        });
    });
};

SSO.prototype.isLoggedIn = function () {
    if (!this.jwt) {
        return false;
    }
    if (!this.jwt.isValid()) {
        this.clearJWT(false);
        return false;
    }
    return this.jwt.getClaim('svc') === this.service && this.jwt.getClaim('login');
};

SSO.prototype.getServiceSpecificToken = function (service) {
    var key = service + '_jwt_token';
    var jwt = this.getSavedJWT(key);
    if (jwt) {
        if (jwt.isValid() && jwt.getClaim('svc') === service) {
            return jwt;
        }
        if (storage) {
            storage.removeItem(key);
        }
    }
    return null;
};

SSO.prototype.getLastToken = function () {
    var key = 'last_jwt_token';
    var jwt = this.getSavedJWT(key);
    if (jwt) {
        if (jwt.isValid()) {
            return jwt;
        }
        if (storage) {
            storage.removeItem(key);
        }
    }
    return null;
};

SSO.prototype.init = function () {
    var _this7 = this;

    return new Promise(function (resolve, reject) {
        if (window) {
            var token = decodeURIComponent(window.location.search.replace(new RegExp('^(?:.*[&\\?]' + encodeURIComponent('token').replace(/[.+*]/g, '\\$&') + '(?:\\=([^&]*))?)?.*$', 'i'), '$1'));
            if (token) {
                console.log('[SSO] JWT token acquired: ' + token);
                _this7.setJWTFromRaw(token);
                if (_this7.options.jwt_acquire_callback) {
                    _this7.options.jwt_acquire_callback(_this7.jwt);
                }
            }
        }

        var jwt = _this7.getServiceSpecificToken(_this7.service);
        if (jwt) {
            console.log('[SSO] Found existing token for service: ' + _this7.service);
            _this7.setJWT(jwt);
            return resolve();
        }

        jwt = _this7.getLastToken();
        if (jwt) {
            console.log('[SSO] Found existing token for other service');
            return _this7.loginWithToken(jwt.getRaw()).then(function () /* jwt */{
                return resolve();
            }).catch(function (err) {
                return reject(err);
            });
        }
        _this7.jwt = null;
        return resolve();
    });
};

SSO.prototype.JWT = JWT;

module.exports = SSO;