# sso helper

## install using

`$ npm install --save git+git@bitbucket.org:allbin/sso-helper.git`

## example usage

```
import SSO from 'sso-helper';

service = 'name-of-service';
sso_options = {
	token_provider_uri: 'url',
	renewal_check_interval: 60, // seconds
}

window.sso = new SSO(service, sso_options);
```
